﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Background_Decarabia
{
    public static class Constants
    {
        //Locations of Elements
        public const string temporaryClassName = "._vt._3u._vu";
        // public const string loginPinterest = ".lightGrey.headerLoginButton.active";
        //new loginPinterest:
        public const string loginPinterest = ".id";
        public const string loginPlaceholder = "id";
        public const string passwordPlaceholder = "password";
        public const string loginName = "DecarabiaC@gmail.com";
        public const string passwordContent = "Soccerpro2";
        public const string loginButton = ".red.SignupButton.active";
        // public const string searchBar = ".Input.Module.field";
        public const string searchBar = ".SearchBoxInput";
        public const string imageLink = ".imageLink";
        public const string imageLinkOffline = "._ta._2c";
        public const string imagesearchin = ".pinLink.pinImageWrapper";
        public const string innerImageTab = ".flashlightSticky";
        public const string tempInnerImage = "._mi._3i._2h._3u"; // = getting src for download
        public const string closeButton = "//button[@aria-label='Close']";
        //public const string imageLinks = "//img["
        public const string gridCenteredClass = ".gridCentered";
        public const string childImages = ".//img";
        //_mi _3i _2h _3u
        //_mi _3i _2h _3u
        public const string newSRC = "(.//img)[3]";
        
        //public const string profilePicFolder = Path.GetDirectoryName(Application.ExecutablePath) + @"\SMSLogs\"; 
        //new dynamic XPATH
        public const string imageListXPATH = "/html/body/div[1]/div[3]/div[1]/div[2]/div/div/div/div/div[3]/div/div/div[1]/div/div/div";
    }
}
