﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using AutoItX3Lib;
using OpenQA.Selenium.Interactions;
using System.Threading;
using System.Drawing;
using System.Net;
using OpenQA.Selenium.Chrome;

//Install-Package Selenium.WebDriver
//Install-Package phantomjs.exe

namespace Background_Decarabia
{
    class Program
    {
        static void Main(string[] args)
        {
            ElementMethods program = new ElementMethods();
            program.promptSearchTerm();
            //Phantom Driver Initialization
            //PhantomJSDriver pDriver = new PhantomJSDriver();
            //PhantomJSDriverService service = PhantomJSDriverService.CreateDefaultService();
            //service.IgnoreSslErrors = true;
            //pDriver.Manage().Window.Maximize();

            program.navigateToWebsite();

           // program.takeScreenshot("navigatedToWebsite");
            program.setSleep(5000, 7000);

            program.login();

            program.takeScreenshot("searchingScreenShot");

            program.searchTopic();

            program.setSleep(3500, 4500);

            program.takeScreenshot("SearchResults");

            program.scrollDownAndDownload();
        }
    }
}
