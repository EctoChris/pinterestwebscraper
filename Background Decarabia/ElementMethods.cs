﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using AutoItX3Lib;
using OpenQA.Selenium.Interactions;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using OpenQA.Selenium.Chrome;
using Background_Decarabia;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;

//Install-Package Selenium.WebDriver -Version 3.6.0
//Install-Package Selenium.Support -Version 3.6.0


namespace Background_Decarabia
{
    class ElementMethods
    {
        //PhantomJSDriver pDriver;
        //PhantomJSDriver tempDriver;
        IWebDriver pDriver;
        AutoItX3 auto;
        Actions action;
        public string searchTerm;

        public ElementMethods()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddUserProfilePreference("download.default_directory", @"C:\Users\Chris\Downloads\Pinterest\RickandMorty");
            //options.AddUserProfilePreference("download.default_directory", @"C:\Users\SaarBaruch");
            chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
            chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");
            pDriver = new ChromeDriver(@"C:\Users\Chris\Documents\chromerDriver", chromeOptions);

            //Declaration of AutoIT
            auto = new AutoItX3();

            //var driverService = PhantomJSDriverService.CreateDefaultService();
            //driverService.SslProtocol = "tlsv1";
            //pDriver = new PhantomJSDriver(driverService);
            // pDriver.Manage().Window.Size = new Size(1920, 1080);

        }
        public bool IsElementPresent(By by, IWebDriver driver1)
        {
            try
            {
                driver1.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public IWebElement refreshElements(By locator, IWebDriver driver, IWebElement imageListNo)
        {
            try
            {
                return imageListNo.FindElement(locator);
            }
            catch (StaleElementReferenceException e)
            {
                Console.WriteLine("Image Needed Refreshing");
                return refreshElements(locator, driver, imageListNo);

            }
        }

        public bool IsElementPresent(IWebElement element, IWebDriver driver1)
        {
            bool test;
            try
            {
                test = element.Displayed;
                return test;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void navigateToWebsite()
        {
             //pDriver.Url = "https://au.pinterest.com/search/pins/?q=rick%20and%20morty&rs=typed&term_meta[]=rick%20and%20morty%7Ctyped";
            pDriver.Url = "https://au.pinterest.com/login/";
            System.Threading.Thread.Sleep(2000);
            pDriver.Navigate();
            System.Threading.Thread.Sleep(1000);
            string url = pDriver.Url;
            Console.WriteLine(url);
        }

        public void promptSearchTerm()
        {
            Console.WriteLine("Enter a term to search for: ");
            searchTerm = Console.ReadLine();
        }

        public void setSleep(int bottomRange, int topRange)
        {
            Random r = new Random();
            int t = r.Next(bottomRange, topRange);
            Thread.Sleep(t);
        }
        public void searchTopic()
        {
            // pDriver.Url = "https://au.pinterest.com/search/pins/?q=rick%20and%20morty&rs=typed&term_meta[]=rick%20and%20morty%7Ctyped";
            //pDriver.Url = "https://au.pinterest.com";
            try
            {
                if (pDriver.FindElement(By.ClassName(Constants.closeButton)) != null)
                {
                    Console.WriteLine("Found Close Button");
                    pDriver.FindElement(By.ClassName(Constants.closeButton)).Click();
                    setSleep(300, 400);
                }
            } catch( Exception e)
            {
                Console.WriteLine("Didnt Find Close Button");
            }

            try
            {
                //Driver.FindElement(By.ClassName(Constants.searchBar)).SendKeys(searchTerm); - failed
                IWebElement searchBar = pDriver.FindElement(By.XPath("//input[@placeholder='Search']"));
                searchBar.SendKeys(searchTerm);
                searchBar.SendKeys(Keys.Enter);
                IWebElement searchDiv = pDriver.FindElement(By.XPath("//div[@role = 'search'"));
                searchDiv.FindElement(By.XPath(".//svg[1]")).Click();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldnt Find SearchBar");
            }

        }
        public void login()
        {
            //setSleep(3000, 8000);

            //Entering Login Details
            pDriver.FindElement(By.Name("id")).SendKeys(Constants.loginName);
            int initialMousePositionX = auto.MouseGetPosX();
            int initialMousePositionY = auto.MouseGetPosY();
            pDriver.FindElement(By.Name("password")).SendKeys(Constants.passwordContent);
            pDriver.FindElement(By.CssSelector(Constants.loginButton)).Click();
            setSleep(3000, 5500);
            Console.WriteLine(pDriver.Url);
        }

        public void takeScreenshot(string nameOfScreenshot)
        {
            Console.WriteLine("Taking Screenshot of " + nameOfScreenshot + "........");
            pDriver.TakeScreenshot().SaveAsFile(@"C:\tempImages\" + nameOfScreenshot + ".png", OpenQA.Selenium.ScreenshotImageFormat.Png);
        }


        public void scrollDownAndDownload()
        {
            int counter = 1;
            int imageCounter = 0;
            string imageName;

            List<string> mainImageHref = new List<string>();
            int count = 0;

            while (true)
            {
                //Step 1: find all child divs where 'data-grid-view' = true
                //Step 2: Search in context of element getting src attribute value
                //Step 3: check if src attribute contains () 'jpg'
                //Step 4: add whole src value to temporaryDownloadList

               

                //-------------------------------------------------------------------
                //string spanCheck = Constants.temporaryClassName + ":nth-of-type(" + counter + ") span:nth-of-type(1)";
                //string imageUrlList = Constants.temporaryClassName + " a:nth-of-type(1)";
                //string imageClick = Constants.temporaryClassName + ":nth-of-type(" + counter + ")";
                //string imageClass = Constants.temporaryClassName;

                Thread.Sleep(1500);
                IList<IWebElement> imageList = new List<IWebElement>();
                List<string> image_HREF_List = new List<string>();
                List<string> temporaryDownloadList = new List<string>();
                List<string> repinCountStringList = new List<string>();
                // IWebElement centeredParentDiv = pDriver.FindElement(By.ClassName(Constants.gridCenteredClass));
                //centeredParentDiv.FindElements(By.XPath(Constants.childImages));
                imageList= pDriver.FindElements(By.XPath("//div[@data-grid-item='true']"));
                Thread.Sleep(1500);

                if (imageList != null)
                {
                    for (int i = 0; i < imageList.Count; i++)
                    {
                        IWebElement href = refreshElements(By.XPath(".//a"), pDriver, imageList[i]);
                        // IWebElement href = imageList[i].FindElement(By.CssSelector(".pinLink.pinImageWrapper"));
                        //IWebElement repinCount = imageList[i].FindElement(By.CssSelector(".socialMetaCount.repinCountSmall > span"));
                        //string repinCountString = repinCount.Text;
                        //if (repinCountString.Contains("k"))
                        //{
                        string tmpHref = href.GetAttribute("href").ToString();
                        Console.WriteLine(tmpHref);
                        image_HREF_List.Add(tmpHref);
                        if (!mainImageHref.Contains(tmpHref))
                        {
                            // repinCountStringList.Add(repinCountString);
                            temporaryDownloadList.Add(tmpHref);
                            mainImageHref.Add(tmpHref);
                            count++;
                        }
                    }

                }

                //New Window
                setSleep(1800, 2500);
                auto.Send("{CTRLDOWN}");
                Thread.Sleep(200);
                auto.Send("n");
                Thread.Sleep(200);
                auto.Send("{CTRLUP}");
                setSleep(800, 1300);
                pDriver.SwitchTo().Window(pDriver.WindowHandles.Last());
                setSleep(800, 1300);



                // foreach (string imageHref in temporaryDownloadList)  
                for (int i = 0; i < temporaryDownloadList.Count; i++)  //Load each image URL
                {
                    imageCounter++;
                    pDriver.Url = temporaryDownloadList[i];
                    // imageName = searchTerm + " No " + imageCounter + " with " + repinCountStringList[i];
                    imageName = searchTerm + "No " + imageCounter;
                    setSleep(2400, 2900);
                   // bool flashlightEnabledFound = IsElementPresent(By.XPath(Constants.newSRC), pDriver);
                   // if (flashlightEnabledFound == true)
                   // {
                        IWebElement innerImage = pDriver.FindElement(By.XPath(Constants.newSRC));
                        string srcUrl = innerImage.GetAttribute("src").ToString();
                        Console.WriteLine("The url of the inner src is : " + srcUrl);
                        Console.WriteLine(@"


");
                    
                        Thread.Sleep(2000);
                        using (WebClient client = new WebClient())
                        {
                            client.DownloadFileAsync(new Uri(srcUrl), @"C:\tempImages\" + imageName + ".jpg");
                        }
                        pDriver.Navigate().GoToUrl(srcUrl); //download image here
                        Thread.Sleep(2000);
                  //  }
                }

                repinCountStringList = null;
                Thread.Sleep(2000);
                pDriver.Close();
                try
                {
                    pDriver.SwitchTo().Window(pDriver.WindowHandles.Last());
                }
                catch (Exception e)
                {
                    Console.WriteLine("unhandled exception");
                }

                Thread.Sleep(550);

                auto.MouseWheel("down", 11);
                Thread.Sleep(6400);
            }
        }
    }
}

